%global _empty_manifest_terminate_build 0
Name:		python-pyzabbix
Version:	1.3.1
Release:	1
Summary:	Zabbix API Python interface
License:	LGPL-2.1
URL:		http://github.com/lukecyca/pyzabbix
Source0:	https://files.pythonhosted.org/packages/87/88/736d7926478dc81e061404a6a2fffd8ea5a019acff0f31be623d4215ef6c/pyzabbix-1.3.1.tar.gz
BuildArch:	noarch

%description
PyZabbix is a Python module for working with the Zabbix API.

%package -n python3-pyzabbix
Summary:	Zabbix API Python interface
Provides:	python-pyzabbix = %{version}-%{release}
BuildRequires:	python3-setuptools
Requires:	python3-requests
Requires:	python3-packaging
%description -n python3-pyzabbix
PyZabbix is a Python module for working with the Zabbix API.

%package help
Summary:	Development documents and examples for pyzabbix
Provides:	python3-pyzabbix-doc
%description help
PyZabbix is a Python module for working with the Zabbix API.

%prep
%autosetup -n pyzabbix-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pyzabbix -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Aug 21 2024 xuhe <xuhe@kylinos.cn> - 1.3.1-1
- Update version to 1.3.1
- chore: setup release-please.
- ci: add python 3.12 to the test matrix.
- docs: add link to the zappix project.
- chore: upgrade pre-commit hooks.
- refactor: reenable logging-fstring-interpolation.
- chore: upgrade pre-commit hooks.
- ci: create release note on tag.

* Wed Apr 12 2023 wubijie <wubijie@kylinos.cn> - 1.3.0-1
- Update package to version 1.3.0

* Fri Nov 04 2022 wangjunqi <wangjunqi@kylinos.cn> - 1.2.1-1
- Update package to version 1.2.1

* Mon Jul 19 2021 mrcangye <mrcangye@email.cn> - 1.0.0-1
- Init Package

